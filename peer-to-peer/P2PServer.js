const WebSocket = require("ws");
const nicoCoin = require('../models/Blockchain')
const P2P_PORT = process.env.P2P_PORT || 5000;
const peers = process.env.PEERS ? process.env.PEERS.split(",") : [];

const TYPES = {
  blockchain: "blockchain",
  transaction: "transaction",
};

class P2PServer {
  constructor(blockchain) {
    this.blockchain = blockchain;
    this.sockets = [];
  }

  perpetuateBlockchain() {
    this.sockets.forEach((socket) => {
      this.sendBlockchain(socket);
    });
  }

  perpetuateTransaction(transaction) {
    this.sockets.forEach((socket) => {
      this.sendTransaction(socket, transaction);
    });
  }

  sendBlockchain(socket) {
    socket.send(
      JSON.stringify({
        type: TYPES.blockchain,
        blockchain: this.blockchain,
      })
    );
  }

  sendTransaction(socket, transaction) {
    socket.send(
      JSON.stringify({
        type: TYPES.transaction,
        transaction: transaction,
      })
    );
  }

  connectSocket(socket) {
    this.sockets.push(socket);
    console.log("Socket sucessfully connected!");
    this.messageHandler(socket);
    this.sendBlockchain(socket);
  }

  connectToPeers() {
    peers.forEach((peer) => {
      const socket = new WebSocket(peer);
      socket.on("open", () => this.connectSocket(socket));
    });
  }

  messageHandler(socket) {
    socket.on("message", (message) => {
      const data = JSON.parse(message);

      switch (data.type) {
        case TYPES.transaction:
          this.blockchain.addTransaction(data.transaction);
          break;

        case TYPES.blockchain:
          this.blockchain.replaceChain(data.blockchain);
      }
    });
  }

  listen() {
    const server = new WebSocket.Server({ port: P2P_PORT });
    server.on("connection", (socket) => this.connectSocket(socket));
    this.connectToPeers();

    console.log(`You are connected peer to peer on port ${P2P_PORT}`);
  }
}

module.exports = new P2PServer(nicoCoin);
