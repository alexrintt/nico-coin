const Wallet = require("../cryptography/Wallet");
const wallet = require("express").Router();

wallet.get("/generate", (req, res) => {
  const keys = new Wallet().generateMyKey();
  res.send(keys);
});

module.exports = wallet;
