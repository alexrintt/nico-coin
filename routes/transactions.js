const Transaction = require("../models/Transaction");
const Wallet = require("../cryptography/Wallet");
const peerToPeer = require("../peer-to-peer/P2PServer")
const nicoCoin = require("../models/Blockchain");

const transaction = require("express").Router();

transaction.get("/", (req, res) => {
  res.json(nicoCoin.pendingTransactions);
});

transaction.post("/", (req, res) => {
  const myKey = new Wallet().getKeyFromPrivate(req.body.fromAccount);
  const walletAdress = myKey.getPublic("hex");

  const transaction = new Transaction(
    walletAdress,
    req.body.toAccount,
    req.body.amount,
    Date.now()
  );

  transaction.signTransation(myKey);
  nicoCoin.addTransaction(transaction);
  peerToPeer.perpetuateTransaction(transaction);

  res.redirect(200, "/transactions");
});

module.exports = transaction