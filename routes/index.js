const routes = require("express").Router();
const blockchain = require("./blockchain");
const wallet = require("./wallet");
const transactions = require("./transactions");

routes.use("/blockchain", blockchain);
routes.use("/wallet", wallet)
routes.use("/transactions", transactions)

module.exports = routes;
