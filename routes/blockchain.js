const nicoCoin = require("../models/Blockchain");
const peerToPeer = require("../peer-to-peer/P2PServer");
const blockchain = require("express").Router();

blockchain.get("/", (req, res) => {
  res.json(nicoCoin.chain);
});

blockchain.get("/safe", (req, res) => {
  res.json(
    nicoCoin.tamperingCheck()
      ? "This blockchain is safe."
      : "This blockchain was tampered."
  );
});

blockchain.get("/balance", (req, res) => {
  const totalAmount = nicoCoin.getAccountBalance(req.body.account);
  res.json(`Your account balance is ${totalAmount} NicoCoin's`);
});

blockchain.post("/mine", (req, res) => {
  const hashBlock = nicoCoin.proofOfWork(req.body.toAccount);
  peerToPeer.perpetuateBlockchain();
  res.json(`Your block was mined: ${hashBlock}`);
});

module.exports = blockchain;
