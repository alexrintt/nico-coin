<p align="center">
  <img width="150" src="https://user-images.githubusercontent.com/66505477/193030725-59c105d9-ddea-471b-8add-849b69a008ac.png#gh-light-mode-only">
  <img width="150" src="https://user-images.githubusercontent.com/51419598/193356346-37952d9f-074e-4bbe-94c4-a2dad541fc4d.png#gh-dark-mode-only">
</p>

<h1 align="center">NicoCoin</h1>
<p>This project it's a self implementation of a blockchain with just some concepts applied.
I getting used to Web3 things so I thought that could be a good start point simulate my own cryptocurrency.
I will list and explain everything about NicoCoin.
Concepts, how could you test in your house and how it's the flow inside a P2P descentralized network.
So, let's go!
</p>

## Concepts Applied
<ul>
  <li>P2P Network.</li>
  <li>Chain Validation.</li>
  <li>Unique Wallet Generation.</li>
  <li>Asymmetric Cryptography.</li>
  <li>Unique Transactions.</li>
  <li>Mining Rewards.</li>
  <li>Blockchain Sync.</li>
  <li>Blockchain Tampering Check.</li>
  <li>Transactions Tampering Check.</li>
  <li>Pending Blocks to Mine.</li>
</ul>

## How to install and test in your local enviroment

First thing it's to clone the repository:
```
git clone https://github.com/Nicochess/NicoCoin.git && cd NicoCoin
```

Then, install the dependencies using:
```
npm install
```

Now, it's everything already installed, you could start a blockchain.
```
npm start
```

To open a peer into a network, you could run in another terminal:
```
npm run dev:1 or npm dev:2
```

If you send a request to whatever port on the network, will be perpetuate to others peers. If you intereact with the blockchain and later enter on the P2P network, the terminal will be syncronized and get the actual instance of blockchain and datas who was perpetuate. Have a try!

## Short Explanation about Blockchain

Just to you undestand, what you are doing basically are joining in a P2P network. What is the meaning of that?
There is no centralized information in blockchain, so, each node it's a peer holding pieces of information. Once you connect to the network you will automatically download a instance of a blockchain locally and be syncronized with the entire blockchain nodes. Everything that happens will be perpetuate through the network if the nodes reach consensus that the information it's valid.

## Routes

###   GET /blockchain
The response will return an actual chain of blocks to you.  
*Response*.
```json
[
  {
    "timestamp": "10/10/10",
    "transactions": "Random Data and Type",
    "previousHash": "0",
    "nonce": 0,
    "hash": "9c4d39c3ffca3751f8a7bcc29e7e101aeb3f47b8df3603b44aae9a6ce0945eb0"
  }
]
```

### GET /blockchain/balance
The response will return your worth value based on your transactions.  
*Request*.
```json
{
  "account": "Your public hash"
}
```

*Response*.
```
Your account balance is 200 NicoCoin's
```

### GET /blockchain/safe
It's just a check to see if all the blockchain was modified.  
*Response if is safe*.
```
This blockchain is safe.
```

*Response if was tampering*.
```
This blockchain was tampered.
```

### POST /blockchain/mine
You send a request and start mining a hash to confirm a pending transactions, send it to the blockchain and get the prize.  
*Request*.
```json
{
  "toAccount": "Your public hash."
}
```

*Response*
```
Your block was mined 9c4d39c3ffca3751f8a7bcc29e7e101aeb3f47b8df3603b44aae9a6ce0945eb0
```

### GET /transactions
You will receive an array with all pending transactions that have to be mined.  
*Response*.
```json
[
  {
    "fromAccount": "04f2cef2ed877316b4a28d6257521a6494e4d79cde2ff8200ef0555eb2fd3bc758723574fa18bf0ef745e202085a913e9656d1a713e1a80fec702cbc111168bf4f",
    "toAccount": "042d907db1236e9a7015b094d9f1e620afb78b27d6ae0467739bc6e2fe62c95065fd80ea84d736510b3f84740f053b681a6726aaa40afc3c43a4521294b00c85a6",
    "amount": "1000",
    "timestamp": 1664490021189,
    "signature": "304402201013c076d4ae601e73c1368172a93fc53c52a6dd773d54be5fdda8f02aeb143e02206c2a7c1237d3b947968051f6ddfeb5da7cd250f131d100e02f5b68c7eb6969d7"
  }
]
```

### POST /transactions
You will send a request with the data to create an transaction that will be perpetuate through the blockchain to be mined.  
*Request*.
```json
{
  "fromAccount": "Your public hash",
  "toAccount": "Account public hash",
  "amount": "1000"
}
```

## How it's the flow inside blockchain?

Well, first thing you need it's a wallet, so you could easily generate a wallet sending a GET request on endpoint **/wallet/generate**. Once you have a wallet, you can mine blocks or realize transactions. The mine process is a way to guarantee that a transaction is valid before you push to the chain and perpetuate through the network. You will dedicate computing power to solve the encryption and get coins from the system because of that. Nothing happens inside a blockchain without verification and consensus. All nodes should agree with hashes and verification when a data start perpetuate. 

## About the project

This is a really small implementation, just to you start to undestand how a blockchain works in a resume. I will provide some resources to you understand even more about the concept of a blockchain and maybe you could implement a blockchain for yourself. Feel free to open an issue or a pull request to contribute with the project.  
- [Undestand about Mining](https://www.youtube.com/watch?v=o1gOyhU6XEw).
- [How data is stored in blockchain](https://www.youtube.com/watch?v=5Uj6uR3fp-U).
- [What is Zero Knowledge Proof?](https://www.youtube.com/watch?v=OcmvMs4AMbM)
- [6 Steps of Blockchain Storage](https://www.youtube.com/watch?v=zovwM4jeYMk).
- [Use cases for Blockchains](https://www.youtube.com/watch?v=aQWflNQuP_o).
- [Proof-of-Stake (vs proof-of-work)](https://www.youtube.com/watch?v=M3EFi_POhps).
- [How does a blockchain work](https://www.youtube.com/watch?v=SSo_EIwHSd4).
- [What is P2P?](https://www.youtube.com/watch?v=V6RTR1eFnWM)
  
You could reach me out on Discord, **Nicochess#6374**.
