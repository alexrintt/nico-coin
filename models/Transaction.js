const Encrypting = require("../cryptography/Encrypting");
const Wallet = require("../cryptography/Wallet");

class Transaction {
  constructor(fromAccount, toAccount, amount, timestamp) {
    this.fromAccount = fromAccount;
    this.toAccount = toAccount;
    this.amount = amount;
    this.timestamp = timestamp;
  }

  generateHash() {
    return new Encrypting().SHA256(
      this.fromAccount + this.toAccount + this.amount + this.timestamp
    );
  }

  signTransation(signKey) {
    if (signKey.getPublic("hex") !== this.fromAccount) {
      throw new Error("Isn't right signing to others wallet.");
    }

    const sign = signKey.sign(this.generateHash(), "base64");
    this.signature = sign.toDER("hex");
  }

  integrityCheck(signature) {
    if (this.fromAccount === null) return true;

    if (!signature || signature.length === 0) {
      throw new Error("This transaction has no signature.");
    }

    const publicKey = new Wallet().getKeyFromPublic(this.fromAccount);
    return publicKey.verify(this.generateHash(), signature);
  }
}

module.exports = Transaction;
