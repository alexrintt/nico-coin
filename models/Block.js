const Encrypting = require("../cryptography/Encrypting");

class Block {
  constructor(timestamp, transactions, previousHash = "") {
    this.timestamp = timestamp;
    this.transactions = transactions;
    this.previousHash = previousHash;
    this.nonce = 0;
    this.hash = this.generateHash();
  }

  generateHash() {
    return new Encrypting().SHA256(
      this.previousHash +
      this.timestamp +
      JSON.stringify(this.transactions) +
      this.nonce
    );
  }

  verifyTransactions() {
    for (const transaction of this.transactions) {
      if (!transaction.integrityCheck(transaction.signature)) {
        return false;
      }
    }
    return true;
  }

  miningBlock(difficulty) {
    while (
      this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")
    ) {
      this.nonce++;
      this.hash = this.generateHash();
    }

    return this.hash;
  }
}

module.exports = Block;
