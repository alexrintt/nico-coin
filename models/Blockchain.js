const Block = require("./Block");
const Transaction = require("./Transaction");

class Blockchain {
  constructor() {
    this.chain = [this.genesis()];
    this.difficulty = 4;
    this.pendingTransactions = [];
    this.miningPrize = 100;
  }

  genesis() {
    return new Block("10/10/10", "Random Data and Type", "0");
  }

  getLastBlock() {
    return this.chain[this.chain.length - 1];
  }

  addTransaction(transaction) {
    const transact = new Transaction(
      transaction.fromAccount,
      transaction.toAccount,
      transaction.amount,
      transaction.timestamp
    );

    if (!transaction.fromAccount || !transaction.toAccount) {
      throw new Error("A transaction must have account from or to addressed.");
    }

    transact.integrityCheck(transaction.signature);
    this.pendingTransactions.push(transaction);
  }

  proofOfWork(prizeAccount) {
    let block = new Block(
      Date.now(),
      this.pendingTransactions,
      this.getLastBlock().hash
    );

    this.pendingTransactions = this.pendingTransactions.slice(
      1,
      this.pendingTransactions.length
    );

    const hash = block.miningBlock(this.difficulty);
    this.chain.push(block);

    this.pendingTransactions = [
      new Transaction(null, prizeAccount, this.miningPrize),
    ];

    return hash;
  }

  getAccountBalance(publicKey) {
    let balance = 0;
    for (const block of this.chain) {
      for (const transaction of block.transactions) {
        if (transaction.fromAccount === publicKey) {
          balance -= Number(transaction.amount);
        }

        if (transaction.toAccount === publicKey) {
          balance += Number(transaction.amount);
        }
      }
    }

    return balance;
  }

  replaceChain(blockchain) {
    if (
      blockchain.chain.length <= this.chain.length &&
      blockchain.pendingTransactions.length <= this.pendingTransactions.length
    ) {
      return;
    }

    this.chain = blockchain.chain;
    this.pendingTransactions = blockchain.pendingTransactions;
  }

  tamperingCheck() {
    for (let i = 1; i < this.chain.length; i++) {
      let currentBlock = this.chain[i];
      let previousBlock = this.chain[i - 1];

      if (!currentBlock.verifyTransactions()) {
        return false;
      }

      if (currentBlock.hash !== currentBlock.generateHash()) {
        return false;
      }

      if (currentBlock.previousHash !== previousBlock.hash) {
        return false;
      }
    }
    return true;
  }
}

module.exports = new Blockchain();
