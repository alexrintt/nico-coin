const forge = require("node-forge");

class Encrypting {
  SHA256(...keys) {
    const md = forge.md.sha256.create();
    md.update(...keys);
    return md.digest().toHex()
  }
}

module.exports = Encrypting;
