const EC = require("elliptic").ec;
const ec = new EC("secp256k1");

class Wallet {
  generateMyKey() {
    const key = ec.genKeyPair();
    const publicKey = key.getPublic("hex");
    const privateKey = key.getPrivate("hex");

    return { publicKey, privateKey }
  }

  getKeyFromPrivate(key) {
    return ec.keyFromPrivate(key, "hex");
  }

  getKeyFromPublic(key) {
    return ec.keyFromPublic(key, "hex");
  }
}

module.exports = Wallet;
