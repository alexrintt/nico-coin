const peerToPeer = require("./peer-to-peer/P2PServer");
const routes = require("./routes");
const express = require("express");

const PORT = process.env.PORT || 3000;

const app = express();
app.use(express.json());

app.use(routes);

app.listen(PORT, () => {
  console.log(`Server is listening on PORT ${PORT}.`);
});

peerToPeer.listen();
